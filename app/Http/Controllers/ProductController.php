<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Cart_Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Product;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function prueba(Request $request){

        $products =DB::table('products')->where('name','product name')->get();
        return view('products')->with('products',$products);
    }


    public function show(Request $request){
        $value = empty($request->input('price'))?'0':$request->input('price') ;

        $all = DB::select(DB::raw("select * from products where price > $value"));

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($all);
        $perPage = 2;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $products= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $products->setPath($request->url());

        return view('products')->with('products',$products);
    }
    public function addToCart($id){
        $product = Product::find($id);

        Log::error('Error'. $id);
        Log::warning('warning'. $id);
        Log::notice('notice'. $id);
        Log::info('info'. $id);
        Log::debug('debug'. $id);

        Log::channel('buy')->info('Ejemplo log');


        if(!$product) {
            abort(404);
        }

        $user = Auth::user()->id;
        $cart_db = Cart::where("user_id",$user)->first();
        if ( empty($cart_db) ){
            $cart_db = new Cart();
            $cart_db->user_id = $user;
            $cart_db->save();
        }

        $cart = session()->get('cart');

        if(!$cart) {
            $cart = [
                $id => [
                    "name" => $product->name,
                    "quantity" => 1,
                    "price" => $product->price,
                    "image" => $product->image
                ]
            ];

            $cart_item = new Cart_Item();
            $cart_item->cart_id=$cart_db->id;
            $cart_item->product_id=$id;
            $cart_item->quantity=1;
            $cart_item->save();

            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        if(isset($cart[$id])) {
            $cart[$id]['quantity']++;
            $cart_item = Cart_Item::where([
                ['cart_id',$cart_db->id],
                ['product_id',$id],
            ])->first();
            $cart_item->quantity=$cart[$id]['quantity'];
            $cart_item->update();
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        $cart[$id] = [
            "name" => $product->name,
            "quantity" => 1,
            "price" => $product->price,
            "image" => $product->image
        ];

        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function cart()
    {
        return view('cart');
    }

    public function update(Request $request){
        if($request->id && $request->quantity){
            $cart = session()->get('cart');
            $cart[$request->id]["quantity"] = $request->quantity;
            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function remove(Request $request){
        if($request->id) {
            $cart = session()->get('cart');
            if(isset($cart[$request->id])) {
                unset($cart[$request->id]);
                session()->put('cart', $cart);
            }
            session()->flash('success', 'Product removed successfully');
        }
    }
    public function venta(Request $request){

    }

}
