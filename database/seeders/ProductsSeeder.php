<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'product name',
            'price' => 12.5,
            'stock' => 5,
            'description' => 'description product',
            'image' => base64_encode(file_get_contents('/home/itb/Descargas/tux.jpeg')),
        ]);

        DB::table('products')->insert([
            'name' => 'product name',
            'price' => 12.5,
            'stock' => 5,
            'description' => 'description product',
            'image' => base64_encode(file_get_contents('/home/itb/Descargas/tux.jpeg')),
        ]);
        //
    }
}
